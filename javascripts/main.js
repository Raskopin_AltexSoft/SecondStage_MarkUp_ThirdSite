$(document).ready(function(){

    var layer = $('#layer');
    var layerhead = $('#layer-head');


    layer.click(function(){
        //$(this).fadeOut('fast');
        //$('#owl2 .owl-dots, #owl3 .owl-dots, #owl4 .owl-dots').toggle();
        //$('#owl2 .owl-nav, #owl3 .owl-nav, #owl4 .owl-nav').toggle();
        //$('.likes').toggle();
        //$('footer>img').toggle();
        if ($("footer > img").is(":visible")) {

        };
        if ($(".hamburger").is(":visible"))
        {
            $(".hamburger").toggle();
        }
    });

    layerhead.click(function(){
       //$(layer).fadeOut('fast');
        //$(this).fadeOut('fast');
        //if ($("footer > img").is(":visible")) {

        //};
        if ($(".hamburger").is(":visible"))
        {
            $(".hamburger").toggle();
        }
    });

    $('.side-title').click(function(e){
        e.preventDefault();
        $(".hamburger").toggle();
    });

    $('#nav-icon3').click(function(){
        $(".hamburger").toggle();
       // $('header').fadeIn('fast');
      //  $('*').fadeIn('fast');
        $(".sub-menu").hide();
        if ($(".hamburger").is(":visible"))
        {
            var menu = $('#nav-icon3');
            var top = $(menu).offset().top;
            $('body,html').animate({scrollTop: top-30}, 500);
        }
    });


    $("#owl1").owlCarousel({
        items:1,
        loop:true,
        dots: true,
        autoplay: true
    });


    $("#owl2").owlCarousel({
        //items:1,
        autoplay: false,
        margin: 15,
        stagePadding: 100,
        loop: true,
        responseClass: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            770: {
                items: 3
            },
            1000: {
                items: 4
            },
            1200: {
                items: 5,
                stagePadding: 0
            },
            1400: {
                items: 6,
                stagePadding: 0
            }

        }
    });

    $("#owl3").owlCarousel({
        items:1,
        autoplay: false,
        margin: 15,
        stagePadding: 100,
        loop: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            770: {
                items: 4,
                stagePadding: 0
            },
            1000: {
                items: 4,
                stagePadding: 0
            },
            1200: {
                items: 5,
                stagePadding: 0
            },
            1400: {
                items: 6,
                stagePadding: 0
            }

        }
    });

    $("#owl4").owlCarousel({
        items:1,
        autoplay: false,
        margin: 15,
        stagePadding: 100,
        loop: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            770: {
                items: 4,
                stagePadding: 0
            },
            1000: {
                items: 4,
                stagePadding: 0
            },
            1200: {
                items: 5,
                stagePadding: 0
            },
            1400: {
                items: 6,
                stagePadding: 0
            }

        }
    });

    $(".sub-menu-parent a").click(function (e) {
        e.preventDefault();
        $(".sub-menu").toggle();
        if ($(".sub-menu-parent>a>span:nth-child(2)").text() == "+")
        {
            $(".sub-menu-parent>a>span:nth-child(2)").text("-");
        }
        else
        {
           $(".sub-menu-parent>a>span:nth-child(2)").text("+");
        }
    });

});

